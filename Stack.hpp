#include "linkedlist.hpp"
#include "ListNode.hpp"

class Stack{
private:
LinkedList *lifo;
ListNode *top;

public:
Stack();
~Stack();
void push(int value);
int peek();
int pop();
int size();
};