#include "ListNode.hpp"
#include "Stack.hpp"
#include <iostream>

ListNode::ListNode(int value){
    this->value = value;
    this->next = nullptr;
}

ListNode::ListNode(){
    this->next = nullptr;
}

ListNode::~ListNode(){
    std::cout<<value<<" deleted\n";
}

Stack::Stack(){
    head = nullptr;
    top = nullptr;
}

Stack::~Stack(){
    ListNode *tmp = head;
    while(head != nullptr){
        tmp = head;
        head = head->next;
        delete tmp;
    }
}

void Stack::push(int value){
    if(head == nullptr){
        head = new ListNode(value);
    }
    else{
        ListNode *n = new ListNode(value);
        n->next = head;
        head = n;
    }
}

int Stack::peek(){
    if(head == nullptr) return 0;
    else{
        return head->value;
    }
}

int Stack::pop(){
    if(head == nullptr){
        return 0;
    }
    else{
        ListNode *ret = head;
        int retval = ret->value;
        head = head->next;
        delete ret;
        return retval;
    }
}

int Stack::size(){
    if(head == nullptr) return 0;
    else{
        int count = 1;
        ListNode *ite = head;
        while(ite->next != nullptr){
            ite = ite->next;
            count++;
        }
        return count;
    }
}

void Stack::print(){
    ListNode *ite = head;
    if(ite == nullptr) std::cout<<"\n";
    else{
        while(ite != nullptr){
            std::cout << ite->value << " | ";
            ite = ite->next;
        }
    }
    std::cout << "\n";
}

int main(int argc, char *argv[]){
    Stack *mystack = new Stack();
    for(int i = 0; i < 10; i++){
        mystack->push(i);
    }
    mystack->print();
    std::cout<< mystack->pop() << " popped\n";
    mystack->print();
    delete mystack;
}