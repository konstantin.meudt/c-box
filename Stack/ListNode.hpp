#pragma once
class ListNode{
    public:
        int value;
        ListNode *next;
        ListNode();
        ListNode(int value);
        ~ListNode();
};