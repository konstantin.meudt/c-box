#include "ListNode.hpp"
#pragma once

class Stack{
    private:
        ListNode *head;
        ListNode *top;
    
    public:
        Stack();
        ~Stack();
        void push(int value);
        int peek();
        int pop();
        int size();
        void print();
};