#include "ListNode.hpp"
#pragma once
class LinkedList{
    private:
    ListNode *head;

    public:
    LinkedList();
    ~LinkedList();
    void appendNode(ListNode *node);

    void putInFront(ListNode *node);

    void print();

    void deleteNode(int value);

    ListNode* getNode(int index);
};