#include <iostream>

class Obj{
private:
    int x;
    double y;
public:
    Obj(int x, double y){
        this->x=x;
        this->y=y;
        std::cout<<"used parametrized constr x:" <<x << "\ty:" << y <<'\n';
    }
    Obj(){
        std::cout<<"used std constr\n";
    }
    ~Obj(){
        std::cout<<"obj destroyed: "<<x << '\t' << y << '\n';
    }
    
    void setX(int x){
        this->x = x;
    }
    
    void setY(double y){
        this->y = y;
    }
    
    int getX(){
        return this->x;
    }
    
    double getY(){
        return this->y;
    }
    
    void incrBy(int i){
        this->x += i;
        this->y = y + i;
    }

};
int main(int argc, char *argv[]){
    Obj a(5, 12.0);
    Obj *b = new Obj();
    b->setX(7);
    b->setY(8.0);
    b->incrBy(5);
    std::cout<<b->getX() << '\t' << b->getY() << '\n';
    std::cout<<a.getX() << '\t' << a.getY() << '\n';
    delete b;
}