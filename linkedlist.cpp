#include <iostream>
#include "ListNode.hpp"
#include "linkedlist.hpp"

    ListNode::ListNode(int value){
        this->value = value;
        this->next = nullptr;
    }
    ListNode::ListNode(int value, ListNode *next){
        this->value = value;
        this->next = next;
    }
    ListNode::~ListNode(){
        this->next = nullptr;
        std::cout << "deleted " << this->value << "\n";
    }

    LinkedList::LinkedList(){
        this->head = nullptr;
    }
    LinkedList::~LinkedList(){
        if(head != nullptr){
            ListNode *ite = head;
            ListNode *next;
            while(ite != nullptr){
                next = ite->next;
                delete ite;
                ite = next;
            }
            head = nullptr;
        }
        std::cout<<"List deleted\n";
    }
    void LinkedList::appendNode(ListNode *node){
        if(this->head == 0){
            this->head = node;
        }
        else{
            ListNode *ite = head;
            while(ite->next != nullptr){
                ite = ite->next;
            }
            ite->next = node;
        }
    }

    void LinkedList::putInFront(ListNode *node){
        node->next = head;
        head = node;
    }

    void LinkedList::print(){
        ListNode *ite = head;
        while(ite != nullptr){
            std::cout << ite->value << " -> ";
            ite = ite->next;
        }
        std::cout<<"\n";
    }

    void LinkedList::deleteNode(int value){
        ListNode *ite = head;
        while(ite != nullptr){
            if(ite->next != nullptr && ite->next->value == value){
                ListNode *toDelete = ite->next;
                ite->next = toDelete->next;
                delete toDelete;
            }
            ite = ite->next;
        }
    }

    ListNode* LinkedList::getNode(int index){
        ListNode *ite = head;
        for(int i = 0; i < index; i++){
            if(ite == nullptr) return nullptr;
            ite = ite->next;
        }
        return ite;
    }

int main(){
    LinkedList *list = new LinkedList();
    for(int i = 0; i < 100; i++){
        ListNode *node = new ListNode(i);
        list->appendNode(node);
    }
    ListNode *a = list->getNode(100);
    std::cout << a << "\n";
    list->deleteNode(99);
    list->print();
    delete list;    
}