#pragma once
class ListNode{
    public:
    ListNode *next;
    int value;

    ListNode();
    
    ListNode(int value);
    ListNode(int value, ListNode *next);
    ~ListNode();
};