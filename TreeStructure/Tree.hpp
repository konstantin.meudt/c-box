#include "TreeNode.hpp"
#pragma once
class Tree{
    public:
    TreeNode *root;
    Tree(TreeNode *root);
    Tree();
    ~Tree();

    //Methods
    void insert(int val);
    void insertInSubtree(int val, TreeNode *node);
    void deletePostOrder(TreeNode *node);
    void traverseInOrder(TreeNode *node);
    void traversePreOrder(TreeNode *node);
    void traversePostOrder(TreeNode *node);
};