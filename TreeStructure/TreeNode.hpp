#pragma once
class TreeNode{
    public:
    int value;
    TreeNode *left;
    TreeNode *right;

    TreeNode(int val);
    TreeNode();
    ~TreeNode();
};