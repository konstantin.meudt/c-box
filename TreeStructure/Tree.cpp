#include "Tree.hpp"
#include "Tree.hpp"
#include <iostream>

TreeNode::TreeNode(int val){
    this->value = val;
    this->left = nullptr;
    this->right = nullptr;
}

TreeNode::~TreeNode(){
    std::cout<<"destroyed node: " << value << '\n';
}

TreeNode::TreeNode(){
    this->left = nullptr;
    this->right = nullptr;
}

Tree::Tree(TreeNode *root){
    this->root = root;
}

Tree::Tree(){
    this->root = nullptr;
}

Tree::~Tree(){
    deletePostOrder(root);
    delete root;
    root = nullptr;
}

void Tree::insertInSubtree(int val, TreeNode *node){
    bool valsmallerequal = val <= node->value;
    if(valsmallerequal && node->left != nullptr){
        insertInSubtree(val, node->left);
    }
    else if(!valsmallerequal && node->right != nullptr){
        insertInSubtree(val, node->right);
    }
    else if(valsmallerequal && node->left == nullptr){
        node->left = new TreeNode(val);
    }
    else if(!valsmallerequal && node->right == nullptr){
        node->right = new TreeNode(val);
    }
    else{
        return;
    }
    
}

void Tree::insert(int val){
    if(root == nullptr){
        root = new TreeNode(val);
    }
    else{
        insertInSubtree(val, root);
    }
}

void Tree::traverseInOrder(TreeNode *node){
    if(node != nullptr){
        traverseInOrder(node->left);
        std::cout << node->value << " ";
        traverseInOrder(node->right);
    }
}

void Tree::traversePostOrder(TreeNode *node){
    if(node != nullptr){
        traversePostOrder(node->left);
        traversePostOrder(node->right);
        std::cout << node->value << " ";
    }
}

void Tree::traversePreOrder(TreeNode *node){
    if(node != nullptr){
        std::cout << node->value << " ";
        traversePreOrder(node->left);
        traversePreOrder(node->right);
    }
}

void Tree::deletePostOrder(TreeNode *node){
    if(node != nullptr){
        deletePostOrder(node->left);
        deletePostOrder(node->right);
        if(node->left != nullptr){
            delete node->left;
            node->left = nullptr;
        }
        if(node->right != nullptr){
            delete node->right;
            node->right = nullptr;
        }
    }
}

int main(){
    Tree *tree = new Tree();
    tree->insert(50);
    tree->insert(25);
    tree->insert(75);
    tree->insert(12);
    tree->insert(37);
    tree->insert(62);
    tree->insert(87);
    tree->traverseInOrder(tree->root);
    delete tree;
}
